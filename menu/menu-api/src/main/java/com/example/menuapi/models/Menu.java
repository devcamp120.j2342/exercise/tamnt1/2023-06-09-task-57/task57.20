package com.example.menuapi.models;

public class Menu {
    private String size;
    private String duongKinh;
    private int suon;
    private int salad;
    private int nuoc;
    private String thanhTien;

    public Menu(String size, String duongKinh, int suon, int salad, int nuoc, String thanhTien) {
        this.size = size;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.nuoc = nuoc;
        this.thanhTien = thanhTien;
    }

    public String getSize() {
        return size;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public int getSalad() {
        return salad;
    }

    public int getNuoc() {
        return nuoc;
    }

    public void setNuoc(int nuoc) {
        this.nuoc = nuoc;
    }

    public String getThanhTien() {
        return thanhTien;
    }

    @Override
    public String toString() {
        return "Menu [size=" + size + ", duongKinh=" + duongKinh + ", suon=" + suon + ", salad=" + salad
                + ", thanhTien=" + thanhTien + "]";
    }

}
