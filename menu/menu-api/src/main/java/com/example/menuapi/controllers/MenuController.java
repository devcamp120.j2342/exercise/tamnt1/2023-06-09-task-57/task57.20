package com.example.menuapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.menuapi.models.Menu;
import com.example.menuapi.services.MenuService;

@RestController
@CrossOrigin

public class MenuController {
    @Autowired
    MenuService menuService;

    @GetMapping("/menu")
    public List<Menu> getMenu() {
        return menuService.createMenu();
    }
}
