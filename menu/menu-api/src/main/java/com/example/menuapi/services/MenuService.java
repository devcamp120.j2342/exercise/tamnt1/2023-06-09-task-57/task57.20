package com.example.menuapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.menuapi.models.Menu;

@Service
public class MenuService {
    public List<Menu> createMenu() {
        List<Menu> menuList = new ArrayList<>();

        Menu small = new Menu("S (Small)", "20cm", 2, 200, 2, "150.000VND");
        Menu medium = new Menu("M (Medium)", "25cm", 4, 300, 3, "200.000VND");
        Menu large = new Menu("L (Large)", "30cm", 8, 500, 4, "250.000VND");
        menuList.add(small);
        menuList.add(medium);
        menuList.add(large);

        return menuList;
    }

}
